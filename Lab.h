#pragma once
#ifndef POPBLTKA_LAB_H
#define POPBLTKA_LAB_H
#endif //POPBLTKA_LAB_H

#include "stdint.h"


typedef struct EL {
    void *(*plustwo)(void *);
    void* (*wh_ch)(void *, int vibor_chisla);
    int size;
} EL;



typedef struct array {
    int size;
    void* data;
    EL *type;
} array;

int sum(int a);

int minus_one(int a);

int a_sqare(int a);

int zero(int a);

int plus_one(int a);

int division_on_two(int a);


void *fplustwo(void *a);

void *iplustwo(void *a);

void *fnct_of_two(void* a);

void* wh_ch_int(void* a, int vibor_chisla);

void* wh_ch_flt(void* a, int vibor_chisla);

void* wh_fnct(void* a, int vibor_chisla);



static EL FLT = {fplustwo, wh_ch_flt, sizeof(float)};
static EL INT = {iplustwo, wh_ch_int, sizeof(int)};
static EL FNCT = {fnct_of_two, wh_fnct, sizeof(char*)};
//static EL TYPE = {type_map, wh_type, sizeof(type)};




void *get(array *a, int i);

array* init(int size, EL* type);

array* concatination(array* a, array* b);

array* where(array* a, int vibor_chisla);

array* map(array* a);

