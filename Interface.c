#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Lab.h"

int main() {
    array *a = NULL;
    array *b = NULL;
    array *c = NULL;
    EL *mir = NULL;
    int type = 0;
    int size = 0;
    int size_conc = 0;
    int choose_of_funct = 0;
    int num_funct = 0;
    void* ssilka;
    while(1) {
        printf("Data Type: 0 - int, 1 - float, 2-functoin\n");
        scanf("%d", &type);
        if (type == 0) {
            mir = &INT;
        } else if (type == 1) {
            mir = &FLT;
        } else if (type == 2) {
            mir = &FNCT;
        }
        printf("What is the funct you wanna choose \n1.Map\n2.Where\n3.Conc\n Answer:  ");
        scanf("%d", &choose_of_funct);
        printf("What is the size of an array: ");
        scanf("%d", &size);
        a = init(size, mir);
        if (choose_of_funct == 1) {
            if (type == 0) {
                for (int i = 0; i < size; i++) {
                    printf("Put the %d element: ", i);
                    scanf("%d", &(*((int *) a->data + i)));
                }
                c = map(a);
                for (int i = 0; i < size; i++) {
                    printf("%d ", *((int *) c->data + i));
                }

            } else if (type == 1) {
                for (int i = 0; i < size; i++) {
                    printf("Put the %d element:", i);
                    scanf("%f", &(*((float *) a->data + i)));
                }
                c = map(a);
                for (int i = 0; i < size; i++) {
                    printf("%.2f ", *((float *) c->data + i));
                }

            } else if (type == 2) {

                for (int i = 0; i < size; i++) {
                    printf("What funct would you wanna choose %d:\n 0-sum\n 1-sqare\n 2-minus one\n 3-zero\n4.plus one\n5.division on two\nAnswer: ", i);
                    scanf("%d",&num_funct);
                    if (num_funct == 0){
                        ssilka = sum;
                    }
                    if (num_funct == 1){
                        ssilka = a_sqare;
                    }
                    if (num_funct == 2){
                        ssilka = minus_one;
                    }
                    if (num_funct == 3){
                        ssilka = zero;
                    }
                    if (num_funct == 4){
                        ssilka = plus_one;
                    }
                    if (num_funct == 5){
                        ssilka = division_on_two;
                    }
                    memcpy(get(a, i), &ssilka, a->type->size);

                }
                c=map(a);

                for (int i = 0; i < size; i++) {
                    int peremennaya = (*(int (**) (int))get(c, i))(2);
                    printf("%d ", peremennaya);


                }
            }

            free(a);
            free(c);

        } else if (choose_of_funct == 2) {
            int vibor_chisla = 0;
            if (type == 0) {
                for (int i = 0; i < size; i++) {
                    printf("Put the %d element: ", i);
                    scanf("%d", &(*((int *) a->data + i)));
                }
                c = where(a,vibor_chisla);
                for (int i = 0; i < size; i++) {
                    printf("%d ", *((int *) c->data + i));
                }

            } else if (type == 1) {
                for (int i = 0; i < size; i++) {
                    printf("Put the %d element", i);
                    scanf("%f", &(*((float *) a->data + i)));
                }
                c = where(a,vibor_chisla);
                for (int i = 0; i < size; i++) {
                   printf("%d ",(int)*((float *) c->data + i));
                }
                free(a);
                free(c);

            } else if (type == 2) {
                printf("Choose the numb for foo: ");
                scanf("%d",&vibor_chisla);

                for (int i = 0; i < size; i++) {
                    printf("What funct would you wanna choose %d:\n 0-sum\n 1-sqare\n 2-minus one\n 3-zero\n4.plus one\n5.division on two\nAnswer: ", i);
                    scanf("%d",&num_funct);
                    if (num_funct == 0){
                        ssilka = sum;
                    }
                    if (num_funct == 1){
                        ssilka = a_sqare;
                    }
                    if (num_funct == 2){
                        ssilka = minus_one;
                    }
                    if (num_funct == 3){
                        ssilka = zero;
                    }
                    if (num_funct == 4){
                        ssilka = plus_one;
                    }
                    if (num_funct == 5){
                        ssilka = division_on_two;
                    }
                    memcpy(get(a, i), &ssilka, a->type->size);

                }
                c=where(a,vibor_chisla);

                for (int i = 0; i < size; i++) {
                    int peremennaya = (*(int (**) (int))get(c, i))(3);
                    printf("%d ", peremennaya);

                }
            }

            free(a);
            free(c);

        } else if (choose_of_funct == 3) {

            printf("Choose the size of the adding array: ");
            scanf("%d", &size_conc);
            b = init(size_conc, mir);
            if (type == 0) {
                for (int i = 0; i < size; i++) {
                    printf("Put the %d element of the first array: ", i);
                    scanf("%d", &(*((int *) a->data + i)));
                }
                for (int i = 0; i < size_conc; i++) {
                    printf("Put the %d element of the second array: ", i);
                    scanf("%d", &(*((int *) b->data + i)));
                }
                c = concatination(a, b);
                for (int i = 0; i < size + size_conc; i++) {
                    printf("%d ", *((int *) c->data + i));
                }
            } else if (type ==1) {
                for (int i = 0; i < size; i++) {
                    printf("Put the %d element of the first array: ", i);
                    scanf("%f", &(*((float *) a->data + i)));
                }
                for (int i = 0; i < size_conc; i++) {
                    printf("Put the %d element of the second array: ", i);
                    scanf("%f", &(*((float *) b->data + i)));
                }
                c = concatination(a, b);
                for (int i = 0; i < size + size_conc; i++) {
                    printf("%.2f ", *((float *) c->data + i));
                }


            } else if (type == 2) {
                char *strs[6] = {"sum","sqare","minus_one","zero","plus_one","division_on_two"};
                int strs_nums[size+size_conc];


                for (int i = 0; i < size; i++) {
                    printf("What funct would you wanna choose %d:\n 0-sum\n 1-sqare\n 2-minus one\n 3-zero\n4.plus one\n5.division on two\nAnswer: ", i);
                    scanf("%d",&num_funct);
                    if (num_funct == 0){
                        ssilka = sum;
                        strs_nums[i] = 0;
                    }
                    if (num_funct == 1){
                        ssilka = a_sqare;
                        strs_nums[i] = 1;
                    }
                    if (num_funct == 2){
                        ssilka = minus_one;
                        strs_nums[i] = 2;
                    }
                    if (num_funct == 3){
                        ssilka = zero;
                        strs_nums[i] = 3;
                    }
                    if (num_funct == 4){
                        ssilka = plus_one;
                        strs_nums[i] = 4;
                    }
                    if (num_funct == 5){
                        ssilka = division_on_two;
                        strs_nums[i] = 5;
                    }

                    memcpy(get(a, i), &ssilka, a->type->size);

                }

                for (int i = 0; i < size_conc; i++) {
                    printf("What funct would you wanna choose for the second array %d:\n 0-sum\n 1-sqare\n 2-minus one\n 3-zero\n4.plus one\n5.division on two\nAnswer: ", i);
                    scanf("%d",&num_funct);
                    if (num_funct == 0){
                        ssilka = sum;
                        strs_nums[i+size] = 0;
                    }
                    if (num_funct == 1){
                        ssilka = a_sqare;
                        strs_nums[i+size] = 1;
                    }
                    if (num_funct == 2){
                        ssilka = minus_one;
                        strs_nums[i+size] = 2;
                    }
                    if (num_funct == 3){
                        ssilka = zero;
                        strs_nums[i+size] = 3;
                    }
                    if (num_funct == 4){
                        ssilka = plus_one;
                        strs_nums[i+size] = 4;
                    }
                    if (num_funct == 5){
                        ssilka = division_on_two;
                        strs_nums[i+size] = 5;

                    }
                    memcpy(get(b, i), &ssilka, b->type->size);

                }

                c=concatination(a,b);

                for (int i = 0; i < size+size_conc; i++) {
                    //int peremennaya = (*(int (**) (int))get(c, i))(2);
                    //printf("%d ", peremennaya);
                    printf("%s ",(strs[strs_nums[i]]));


                }
            }
        }
        free(a);
        free(c);
    int try_again = 0;
    printf("\nDo you want to try again?)\nYes-1\nNo-2\n");

    scanf("%d",&try_again);
    if (try_again == 2){
        break;
    }

    }

}


