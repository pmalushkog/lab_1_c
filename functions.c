#include "Lab.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



void *get(array *a, int i) {
    return (char *)a->data+i*a->type->size;
}

int sum(int a) {
    return a+a;
}
int minus_one(int a) {
    return a-1;
}
int a_sqare(int a) {
    return a*a;
}
int zero(int a) {
    return a-a;
}
int plus_one(int a) {
    return a+1;
}
int division_on_two(int a) {
    int res =a/2;
    return res;
}

void* qwe = zero;


void* fnct_of_two(void* a) {
    if ((*(int (**) (int))a)(2) >=10 ) {
        return a;

    } else if ((*(int (**) (int))a)(2) <10 ) {
        return &qwe;
    }

}

int funct_true(int a) {
    return 1;
}

void *fplustwo(void *a) {
    float* res   = malloc(sizeof(float));
    res[0] = *(float *) a + 2;
    return res;
}

void *iplustwo(void *a) {
    int *res = malloc(sizeof(int));
    res[0] = *(int *) a + 2;
    return res;
}

void* wh_ch_int(void* a, int vibor_chisla){
    int *res = malloc(sizeof(int));
    if (*(int* ) a <= 10){
        res[0] = 1;
    }
    if (*(int* ) a > 10){
        res[0] = 0;
    }

    return res;
}



void* wh_ch_flt(void* a, int vibor_chisla) {
    float *res = malloc(sizeof(int));
    if (*(float* ) a <= (float)10){
        res[0] = 1;
    }
    if (*(float* ) a > (float)10){
        res[0] = 0;
    }

    return res;
}

void* true_wh = funct_true;
void* false_wh = zero;

void* wh_fnct(void* a, int vibor_chisla) {
    if ((*(int (**) (int))a)(vibor_chisla) <=5 ) {
        return &true_wh;

    } else if ((*(int (**) (int))a)(vibor_chisla) >5 ) {
        return &false_wh;
    }
}


array* init(int size, EL* type) {
    array* in = calloc(1, sizeof(array));
    in->size = size;
    in->type = type;
    in->data = calloc(size, type->size);
    return in;
}

array* concatination(array* a, array* b){
    array* res_array = init(a->size+b->size,a->type);
    for (int i = 0; i < a->size; i++) {
        void* temp = get(a, i);
        memcpy(get(res_array, i), temp, a->type->size);
    }
    for (int i = a->size; i < b->size+a->size; i++) {
        void* temp = get(b, i-(a->size));
        memcpy(get(res_array, i), temp, b->type->size);
    }
    return res_array;

}

array* where(array* a, int vibor_chisla) {
    array *wh_check = NULL;
    wh_check = init(a->size, a->type);
    for (int i = 0; i < a->size; i++) {
        void* temp = a->type->wh_ch(get(a, i), vibor_chisla);
        memcpy(get(wh_check, i), temp, a->type->size);
        free(temp);
    }
    return wh_check;
}

array* map(array* a) {

    array *plus_two = NULL;
    plus_two = init(a->size, a->type);
    for (int i = 0; i < a->size; i++) {
        void* temp = a->type->plustwo(get(a, i));
        memcpy(get(plus_two, i), temp, a->type->size);

    }
    return plus_two;
}